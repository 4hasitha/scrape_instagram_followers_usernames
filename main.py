from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time, random
from dotenv import load_dotenv
import os
import pandas as pd

load_dotenv()

def login():
    username = driver.find_element_by_xpath("//input[@name='username']")
    username.send_keys(os.environ.get('INSTA_USERNAME'))

    time.sleep(random.randint(1, 5))

    password = driver.find_element_by_xpath("//input[@name='password']")
    password.send_keys(os.environ.get('INSTA_PASSWORD'))

    time.sleep(random.randint(1, 5))

    driver.find_element_by_xpath("//button[@class='sqdOP  L3NKy   y3zKF     ']").click()

    time.sleep(random.randint(3, 5))

    driver.find_element_by_xpath("//button[@class='sqdOP yWX7d    y3zKF     ']").click()

    time.sleep(random.randint(3, 5))

    print("Login Successfully")

options = Options()
if(os.environ.get('SHOW_BROWSER') == 'false'):
    options.add_argument('--headless')
    options.add_argument('--disable-gpu')
driver = webdriver.Chrome('./chromedriver', chrome_options=options)

time.sleep(random.randint(3, 5))
driver.get("https://www.instagram.com/accounts/login/")
time.sleep(random.randint(3, 5))

login()

driver.get("https://www.instagram.com/"+os.environ.get('INSTA_PUBLIC_PROFILE_USERNAME')+"/")

time.sleep(random.randint(2,5))

driver.find_element_by_xpath("//a[@href='/"+os.environ.get('INSTA_PUBLIC_PROFILE_USERNAME')+"/followers/']").click()
time.sleep(random.randint(2, 5))

print("Start scraping followers")

time.sleep(random.randint(1,5))

for x in range(20):
    scroll_y = x*250
    liElement = driver.find_element_by_xpath("//div[@class='isgrP']")
    driver.execute_script("arguments[0].scrollTo(0,"+str(scroll_y)+");", liElement)
    print("Data scraping...")
    time.sleep(x+2)

followers_usernames = driver.find_elements_by_xpath("//span[@class='_7UhW9   xLCgt        qyrsm KV-D4           se6yk       T0kll ']")

print("Finish Scraping. Start to create csv")

list_followers_usernames = []
for follower_username in followers_usernames:
    list_followers_usernames.append(follower_username.text)

df = pd.DataFrame(list_followers_usernames, columns=['follower_username'])
df.to_csv('output/'+os.environ.get('INSTA_PUBLIC_PROFILE_USERNAME')+'_followers.csv', sep=',', encoding='utf-8')

time.sleep(random.randint(2,5))

driver.close()